import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { routing } from './app.routing';

import { CartService } from './cart/cart.service';
import { GetProductDataService } from './item-list/get-product-data.service';

import { AppComponent } from './app.component';
import { ItemListComponent } from './item-list/item-list.component';
import { ItemComponent } from './item/item.component';
import { FilterComponent } from './filter/filter.component';
import { ItemInComponent } from './item-in/item-in.component';
import { CartComponent } from './cart/cart.component';
import { HeaderComponent } from './header/header.component';
import { FilterPipe } from './filter/filter.pipe';

export const firebaseConfig = {
  apiKey: "AIzaSyDWR5DYN1SLcHfAj8wgSWc__zYVeSrqfX8",
  authDomain: "infaretask.firebaseapp.com",
  databaseURL: "https://infaretask.firebaseio.com",
  storageBucket: "infaretask.appspot.com",
  messagingSenderId: "222970016678"
};

@NgModule({
  declarations: [
    AppComponent,
    ItemListComponent,
    ItemComponent,
    FilterComponent,
    ItemComponent,
    ItemInComponent,
    CartComponent,
    HeaderComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    routing
  ],
  providers: [CartService, GetProductDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
