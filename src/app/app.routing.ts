import { Routes, RouterModule } from "@angular/router"

import { ItemListComponent } from "./item-list/item-list.component"
import { ItemInComponent } from "./item-in/item-in.component"
import { CartComponent } from "./cart/cart.component"

const APP_ROUTES: Routes = [
  { path: 'item/:id', component: ItemInComponent },
  { path: 'cart', component: CartComponent },
  { path: '', component: ItemListComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
]

export const routing = RouterModule.forRoot(APP_ROUTES);