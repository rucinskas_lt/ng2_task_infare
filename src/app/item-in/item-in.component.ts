import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';

import { GetProductDataService } from '../item-list/get-product-data.service';
import { Product } from '../item/item';
import { CartService } from '../cart/cart.service';

@Component({
  selector: 'app-item-in',
  templateUrl: './item-in.component.html',
  styleUrls: ['./item-in.component.sass']
})
export class ItemInComponent implements OnDestroy, OnInit {
  private subscription: Subscription;
  private product;
  private cartable;
  private id: number;
  private incart: boolean = false;
      
  constructor(private router: Router, private active: ActivatedRoute, private db: GetProductDataService, private cart: CartService) {
    this.subscription = active.params.subscribe(
      (param: any) => this.id = param['id']
    );
  }

  onAddToCart() {
    this.cart.addItems(this.cartable);
    this.incart = true;
  }

  ngOnInit() {
    this.incart = this.cart.inCart(this.id);
    this.product = this.db.getProductById(this.id);
    this.db.getProductById(this.id).subscribe(
      data => this.cartable = data
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
