import { Component, OnInit } from '@angular/core';
import { CartService } from './cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.sass']
})
export class CartComponent implements OnInit {
  private cartItems = [];

  constructor(private cart: CartService) { }

  ngOnInit() {
    this.cartItems = this.cart.getItems();
  }

  removeItem(item) {
    this.cart.removeItem(item);
  }
}
