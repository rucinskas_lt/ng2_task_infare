import { Injectable } from '@angular/core';
import { Product } from '../item/item'

@Injectable()
export class CartService {
  private cartItems = [];
  public count = 0;

  constructor() { }

  getItems() {
    return this.cartItems;
  }

  inCart(id: number) {
    let ids = [];
    for (let cartItem of this.cartItems) {
      ids.push(cartItem.id.toString());
    }
    if (ids.indexOf(id) === -1) {
      return false;
    } else {
      return true;
    }
  }


  addItems(item) {
    let ids = [];
    for (let cartItem of this.cartItems) {
      ids.push(cartItem.id);
    }
    if (ids.indexOf(item.id) === -1) {
      this.cartItems.push(item);
    }
    this.updateCount();
  }


  removeItem(item) {
    if (this.cartItems.indexOf(item) !== -1) {
      this.cartItems.splice(this.cartItems.indexOf(item), 1);
    }
    this.updateCount();
  }

  updateCount() {
    this.count = this.cartItems.length;
    return this.count;
  }
}
