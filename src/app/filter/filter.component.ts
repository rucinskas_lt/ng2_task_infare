import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.sass']
})
export class FilterComponent implements OnInit {
  @Output() filter = new EventEmitter<string>();

  constructor() { }

  filterList(value) {
    this.filter.emit(value);
  }

  ngOnInit() {

  }
}
