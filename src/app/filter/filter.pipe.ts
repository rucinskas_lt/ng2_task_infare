import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    if (typeof args === 'undefined' || args.length === 0) {
      return value;
    }

    if (value) {
      let resultArray = [];
      for (let item of value) {
        for (let color of item.colors) {
          if (color === args) {
            resultArray.push(item);
          }
        }
      }
      return resultArray;
    }
  }
}
