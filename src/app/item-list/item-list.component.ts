import { Component, OnInit, OnChanges, SimpleChanges, Input, trigger, transition, style, animate } from '@angular/core';
import { Product } from '../item/item';
import { GetProductDataService } from './get-product-data.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.sass'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'scale(0)', opacity: 0}),
          animate('250ms', style({transform: 'scale(1)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'scale(1)', opacity: 1}),
          animate('250ms', style({transform: 'scale(0)', opacity: 0}))
        ])
      ]
    )
  ]
})
export class ItemListComponent implements OnInit, OnChanges {
  private showLoader: boolean;
  private products;
  @Input() filterString:string = '';

  constructor(private db: GetProductDataService) {  }

  filter(filter: string) {
    this.filterString = filter;
  }

  ngOnInit() {
    this.showLoader = true;
    this.products = this.db.getProducts();
    this.products.subscribe(() => this.showLoader = false);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
  }
}
