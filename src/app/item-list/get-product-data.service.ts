import { Injectable } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import { Product } from '../item/item';

@Injectable()
export class GetProductDataService {
  private products: FirebaseListObservable<any>;
  private product: FirebaseObjectObservable<any>;

  constructor(private af: AngularFire) {}

  getProducts() {
    this.products = this.af.database.list('/products');
    return this.products;
  }

  getProductById(id) {
    this.product = this.af.database.object('/products/'+id);
    return this.product;
  }
}
