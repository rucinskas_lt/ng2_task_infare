import { Component, Input } from '@angular/core';
import { Product } from './item';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.sass']
})
export class ItemComponent {
  @Input() p: Product;

  constructor() { }
}
