export class Product {
  colors: any[];
  id: number;
  img: string;
  name: string;
  price: number;
}