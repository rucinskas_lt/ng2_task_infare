import { InfaretaskPage } from './app.po';

describe('infaretask App', function() {
  let page: InfaretaskPage;

  beforeEach(() => {
    page = new InfaretaskPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
